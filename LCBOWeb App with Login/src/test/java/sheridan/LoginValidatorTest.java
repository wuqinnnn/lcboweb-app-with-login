package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {
	@Test
	public void testIsValidLoginRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses"));
	}

	@Test
	public void testIsValidLoginException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ramse"));
	}

	@Test
	public void testIsValidLoginBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ramse"));
	}

	@Test
	public void testIsValidLoginBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ramses"));
	}

	@Test
	public void testIsValidLoginCharcterRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ram123"));
	}

	@Test
	public void testIsValidLoginCharcterException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ramse#"));
	}

	@Test
	public void testIsValidLoginCharcterBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("ram1se#"));
	}

	@Test
	public void testIsValidLoginCharcterBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("ram123"));
	}

}
