package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {

		if (loginName.length() >= 6) {
			return loginName.matches("^[0-9a-zA-Z]*+$");
		}
		return false;
	}
}
